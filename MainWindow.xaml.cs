﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace TodoList
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string fileName = @"..\..\tasksTodo.txt";
        static readonly List<Task> taskList = new List<Task>();

        public MainWindow()
        {
            InitializeComponent();
            LoadFromFile();
        }

        private void LoadFromFile()
        {
            try
            {
                StreamReader sr = new StreamReader(fileName);
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    string[] taskStrings = line.Split(';');
                    string name = taskStrings[0];
                    int dif;
                    if (!int.TryParse(taskStrings[1], out dif))
                    {
                        MessageBox.Show("Difficulty must be a number");
                    }

                    DateTime date;
                    if (!DateTime.TryParse(taskStrings[2], out date))
                    {
                        MessageBox.Show("Due date must be a Date");
                    }

                    string status = taskStrings[3];
                    Task newTask = new Task(name, dif, date, status);
                    taskList.Add(newTask);
                }

                ListViewTask.ItemsSource = taskList;
            }
            catch (IOException e)
            {
                throw e;
            }
        }


        private void BtnAddTask_Click(object sender, RoutedEventArgs e)
        {
            string name = TextBoxTaskName.Text;
            int dif = 2;
            DateTime date = DateTime.Now;
            string status = ComboBoxStatus.Text;
            //if (!int.TryParse(SliderDif.GetValue(DependencyProperty dp).ToString(), out int dif))
            //{
            //    MessageBox.Show("Age must be a number");
            //    return;
            //}

            Task newpTask = new Task(name, dif, date, status);
            taskList.Add(newpTask);

            ListViewTask.ItemsSource = taskList;
            ListViewTask.Items.Refresh();
        }
        private void BtnDeleteTask_Click(object sender, RoutedEventArgs e)
        {
            if (ListViewTask.SelectedIndex == -1)
            {
                MessageBox.Show("Select one item.");
                return;
            }

            taskList.Remove((Task)ListViewTask.SelectedItem);
            ListViewTask.Items.Refresh();
        }

        private void BtnUpdateTask_Click(object sender, RoutedEventArgs e)
        {
            if (ListViewTask.SelectedIndex == -1)
            {
                MessageBox.Show("Select one item.");
                return;
            }

            Task task = (Task)ListViewTask.SelectedItem;
            task.TaskName = TextBoxTaskName.Text;
            if (!int.TryParse(ShowDif.Content.ToString(), out int dif))
            {
                MessageBox.Show("Difficulty must be a number");
                return;
            }

            task.Difficulty = dif;
            DateTime date;
            if (!DateTime.TryParse(TextBoxDueDate.Text.ToString(), out date))
            {
                MessageBox.Show("Due date must be a Date");
            }

            task.DueDate = date;
            task.Status = ComboBoxStatus.Text;

            ListViewTask.Items.Refresh();
        }

        private void ListViewTask_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BtnDeleteTask.IsEnabled = true;
            BtnUpdateTask.IsEnabled = true;

            Task Task = (Task)ListViewTask.SelectedItem;
            if (Task != null)
            {
                TextBoxTaskName.Text = Task.TaskName;
                SliderDif.Value = Task.Difficulty;
                TextBoxDueDate.Text = Task.DueDate.ToString("yyyy-MM-dd");
                ComboBoxStatus.Text = Task.Status;
            }
            ListViewTask.Items.Refresh();
        }

        private void BtnExportAllToFile_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog file = new SaveFileDialog();
            file.Filter = "txt|*.txt";
            if (file.ShowDialog() == true)
            {
                using (StreamWriter sw = File.CreateText(file.FileName))
                {
                    foreach (Task t in taskList)
                    {
                        sw.WriteLine($"{t.TaskName};{t.Difficulty};{t.DueDate.ToString("yyyy-MM-dd")};{t.Status}");
                    }
                }
            }
            
        }
    }
}
