﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoList
{
    class Task
    {
        public string TaskName { get; set; }

        public int Difficulty { get; set; }

        public DateTime DueDate { get; set; }
        public string Status { get; set; }

        public Task(string taskName, int difficulty, DateTime dueDate, string status)
        {
            TaskName = taskName;
            Difficulty = difficulty;
            DueDate = dueDate;
            Status = status;
        }
        public override string ToString()
        {
            return string.Format($"{TaskName} by {DueDate.ToString("yyyy-MM-dd")} / difficulty. {Difficulty}, {Status}");
        }
    }
}
